#!/bin/bash

set -e

. ./support/functions.sh

vault_init() {
  data=$(
    cat <<EOF
{
  "secret_shares": 10,
  "secret_threshold": 5,
  "recovery_shares": 10,
  "recovery_threshold": 5
}
EOF
  )

  http_notoken_req_string PUT "$data" "$VAULT_ADDR/sys/init"
}

execute_init() {
  log_message "Initializing Vault at $VAULT_ADDR..."

  vault_keys=$(vault_init | jq .)
  export VAULT_TOKEN="$(echo $vault_keys | jq -r '.root_token')"
  export AUTH_TOKEN="X-Vault-Token: ${VAULT_TOKEN}"
  echo "$vault_keys"

  log_message "Store Vault Recovery keys and Root token in $AWS_DEFAULT_REGION secrets manager"
  aws secretsmanager create-secret --region $AWS_DEFAULT_REGION --name "cmig-$VAULT_CLUSTER_NAME-keys" --description "Vault Recovery keys and Root Token" --secret-string "$vault_keys"
}

execute_init