###############################################################
# Helper functions.
###############################################################
http_req_string() {
  curl --fail --insecure \
    --header "$AUTH_TOKEN" \
    --request $1 \
    --data "$2" \
    $3
}

http_req_file() {
  curl --fail --insecure \
    --header "$AUTH_TOKEN" \
    --request $1 \
    --data @$2 \
    $3
}

http_req_empty() {
  curl --fail --insecure \
    --header "$AUTH_TOKEN" \
    --request $1 \
    $2
}

http_notoken_req_string() {
  curl --fail --insecure \
    --request $1 \
    --data "$2" \
    $3
}

http_notoken_req_empty() {
  curl --fail --insecure \
    --request $1 \
    $2
}

# Description
#   Outputs a log message with a timestamp.
# Arguments
#   $1 message
log_message() {
  DATETIME=$(date "+%Y-%m-%d %H:%M:%S")
  echo "${DATETIME} :: $1"
}

# Description
#   Receives user confirmation for a given command (yes/no).
#   Returns true if the user answered "yes", false otherwise.
# Arguments
#   $1 Confirmation message.
confirm() {
  read -r -p "${1} [y/N] " response
  case "${response}" in
  [yY][eE][sS] | [yY])
    true
    ;;
  *)
    false
    ;;
  esac
}

# Description
#   Receives user confirmation for a given command (yes/no).
#   Returns true if the user answered "yes", exists the script otherwise.
# Arguments
#   $1 Confirmation message.
confirm_or_exit() {
  read -r -p "${1} [y/N] " response
  case "${response}" in
  [yY][eE][sS] | [yY])
    true
    ;;
  *)
    exit 1
    ;;
  esac
}
