#!/bin/bash

set -e

. ./support/functions.sh

config_kv_v1() {
  log_message "Configuring Secrets Engine for $1 in relative path /cmig/$1/v1 ..."
  app_name=$1

  data=$(
    cat <<EOF
{
  "type": "kv",
  "description": "KV v1 secrets engine for $app_name",
  "config": {
    "default_lease_ttl":"0",
    "max_lease_ttl":"0",
    "force_no_cache": false,
    "options": {
      "version":"1"
    }
  }
}
EOF
  )

  http_req_string POST "$data" "$VAULT_ADDR/sys/mounts/cmig/$app_name/v1"
}

load_secrets() {
  log_message "Loading Secrets for $1 ..."
  app_group=$1

  for file in ../secrets/$app_group/*.json; do
    name=$(basename $file)
    http_req_file POST $file "$VAULT_ADDR/cmig/$app_group/v1/${name%.*}"
    log_message "   $name secret loaded "
  done
}

execute_secrets() {
  config_kv_v1 guidewire
  config_kv_v1 microservices
  load_secrets microservices
}

execute_secrets