#!/bin/bash

set -e

. ./support/functions.sh

config_auth() {
  log_message "Configuring AWS IAM Auth method ..."

  data=$(
    cat <<EOF
{
  "description": "Commonwell AWS IAM authentication method",
  "type": "aws",
  "config": {
    "default_lease_ttl" : "10m",
    "max_lease_ttl" : "60m"
  }
}
EOF
  )

  http_req_string POST "$data" "$VAULT_ADDR/sys/auth/cmig/aws/iam"
}

config_devops_role() {
  log_message "Configuring DevOps $1 Role for $2 ..."

  type=$1
  iam_role_arn=$2

  data=$(
    cat <<EOF
{
  "auth_type": "iam",
  "bound_iam_principal_arn": "$iam_role_arn",
  "ttl": "5m",
  "max_ttl": "30m",
  "policies": [
    "$type"
  ]
}
EOF
  )

  http_req_string POST "$data" "$VAULT_ADDR/auth/cmig/aws/iam/role/$type"
}

config_application_role() {
  log_message "Configuring Application $1 Role for $2 ..."

  app=$1
  iam_role_arn=$2

  data=$(
    cat <<EOF
{
  "auth_type": "iam",
  "bound_iam_principal_arn": "$iam_role_arn",
  "ttl": "10m",
  "max_ttl": "20m",
  "policies": [
    "cmig/secrets/$app"
  ]
}
EOF
  )

  http_req_string POST "$data" "$VAULT_ADDR/auth/cmig/aws/iam/role/$app"
}

load_acl_policies() {
  log_message "Loading ACL policies ..."

  for file in ../acl_policies/*.hcl; do
    name=$(basename $file)
    payload=$(jq -n --arg b "$(cat $file)" '{"policy": $b}')
    http_req_string PUT "$payload" "$VAULT_ADDR/sys/policy/${name%.*}"
    log_message "   $name acl policy loaded "
  done
}

execute_auth() {
  config_auth
  load_acl_policies

  config_devops_role admin $DEVOPS_IAM_ARN
  config_devops_role provisioner $DEVOPS_IAM_ARN

  config_application_role microservice $MS_IAM_ARN
  config_application_role guidewire $GW_IAM_ARN
}

execute_auth